#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
git pull

ls -a | grep "^\..*$" > tmp
comm -13 ignore tmp > tmp2
mv tmp2 tmp
cd ~
cp `cat $SCRIPT_DIR/tmp` $SCRIPT_DIR
cd $SCRIPT_DIR
rm $SCRIPT_DIR/tmp

git add .
git commit -m"Changes to dotfiles"
git push