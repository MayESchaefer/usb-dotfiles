# USB Dotfiles Manager

A simple set of shell scripts to manage dotfiles in a way completely compatible with just plugging in a tiny USB drive and running the script (my use-case).

I found dotbot to be *far* too large and complex for what I actually needed. 550 KiB? *Far* too enterprise for me. Try 550 *bytes*.
(Please note that the 550 byte number was *before* I finished writing this readme. I assume it's more like 1KiB now.)

In all seriousness, I made this because the way dotbot works didn't work as well for just plugging in a USB and running the script. The size is just a bonus.

## Usage

Place all of your dotfiles in the same directory as the scripts. Commit. Push.

(Add the relevant lines to the `ignore` file if you create a `.gitignore`, or any other files beginning with "." that you *don't* want to install on every system.)

You can now run `install.sh` at any time, from any system, to update the local repo and install the dotfiles.

Run `update.sh` to update the local and external repos to match the changes made to the files on your system.
This will *only* affect files matching the filenames in the local repository. If you want to add a new dotfile, you'll have to copy it over manually.

And that's it! Three files, and presto! Dotfiles!
